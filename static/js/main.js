(function ($) {
    "use strict";
    
    // Dropdown on mouse hover
    $(document).ready(function () {
        function toggleNavbarMethod() {
            if ($(window).width() > 992) {
                $('.navbar .dropdown').on('mouseover', function () {
                    $('.dropdown-toggle', this).trigger('click');
                }).on('mouseout', function () {
                    $('.dropdown-toggle', this).trigger('click').blur();
                });
            } else {
                $('.navbar .dropdown').off('mouseover').off('mouseout');
            }
        }
        toggleNavbarMethod();
        $(window).resize(toggleNavbarMethod);
    });
    
    
    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({scrollTop: 0}, 1500, 'easeInOutExpo');
        return false;
    });


    // Tranding carousel
    $(".tranding-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 2000,
        items: 1,
        dots: false,
        loop: true,
        nav : true,
        navText : [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ]
    });


    // Carousel item 1
    $(".carousel-item-1").owlCarousel({
        autoplay: true,
        smartSpeed: 1500,
        items: 1,
        dots: false,
        loop: true,
        nav : true,
        navText : [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ]
    });

    // Carousel item 2
    $(".carousel-item-2").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        margin: 30,
        dots: false,
        loop: true,
        nav : true,
        navText : [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ],
        responsive: {
            0:{
                items:1
            },
            576:{
                items:1
            },
            768:{
                items:2
            }
        }
    });


    // Carousel item 3
    $(".carousel-item-3").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        margin: 30,
        dots: false,
        loop: true,
        nav : true,
        navText : [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ],
        responsive: {
            0:{
                items:1
            },
            576:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:3
            }
        }
    });
    

    // Carousel item 4
    $(".carousel-item-4").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        margin: 30,
        dots: false,
        loop: true,
        nav : true,
        navText : [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ],
        responsive: {
            0:{
                items:1
            },
            576:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });
    
})(jQuery);



// ------------------------------------------ 
    function saveProfile() {
        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var fullName = firstName + ' ' + lastName;

        // Hiển thị tên ở đây hoặc thực hiện các công việc khác cần thiết
        alert('Họ tên của bạn là: ' + fullName);
    }
    // ----------------------------------------- sao chép liên kết--------------------------------
    document.addEventListener("DOMContentLoaded", function() {
        var copyIcon = document.getElementById("copyIcon");
        var copyNotification = document.getElementById("copyNotification");

        copyIcon.addEventListener("click", function() {
            var textArea = document.createElement("textarea");
            textArea.value = window.location.href;

            document.body.appendChild(textArea);
            textArea.select();
            document.execCommand("copy");

            document.body.removeChild(textArea);

            copyNotification.textContent = "Đã sao chép URL!";
            copyNotification.style.display = "block";

            setTimeout(function() {
                copyNotification.style.display = "none";
            }, 3000);
        });
    });

    document.addEventListener("DOMContentLoaded", function() {
        var copyIcon = document.getElementById("copyIcon");
        var copyContainer = document.querySelector(".copy-container");
        var copyTooltip = document.getElementById("copyTooltip");

        // Hiển thị tooltip khi di chuột qua
        copyContainer.addEventListener("mouseover", function() {
            copyTooltip.style.display = "inline-block";
        });

        // Ẩn tooltip khi di chuột ra khỏi phần tử
        copyContainer.addEventListener("mouseout", function() {
            copyTooltip.style.display = "none";
        });

       
    });
// ----------------------------------------- lưu tin--------------------------------



   document.addEventListener("DOMContentLoaded", function() {
       var luuTinContainer = document.getElementById("luuTinContainer");

       luuTinContainer.addEventListener("mouseover", function() {
           luuTinContainer.classList.add("show-tooltip");
       });

       luuTinContainer.addEventListener("mouseout", function() {
           luuTinContainer.classList.remove("show-tooltip");
       });
   });
// ----------------------------------------- sao chép tin--------------------------------
var currentScale = 1.0;

function increaseScale() {
    currentScale += 0.1;
    updateScale();
}

function decreaseScale() {
    currentScale -= 0.1;
    updateScale();
}

function updateScale() {
    console.log('Updating scale to: ' + currentScale);
    document.getElementById('zoomTextContainer').style.transform = 'scale(' + currentScale + ')';
}
// -------------------------------------------


  