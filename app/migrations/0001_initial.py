# Generated by Django 4.2.5 on 2023-11-16 17:21

import ckeditor_uploader.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='img')),
                ('name', models.CharField(max_length=20)),
                ('content', models.TextField(null=True)),
                ('body', models.TextField(blank=True, null=True)),
                ('body1', models.TextField(blank=True, null=True)),
                ('body2', models.TextField(blank=True, null=True)),
                ('body3', models.TextField(blank=True, null=True)),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
            ],
            options={
                'verbose_name_plural': '0.1 Trang Chủ',
            },
        ),
        migrations.CreateModel(
            name='DoiSong',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '3 Đời Sống',
            },
        ),
        migrations.CreateModel(
            name='DuLich',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '6 Du Lịch',
            },
        ),
        migrations.CreateModel(
            name='GiaiTri',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '7 Giải Trí',
            },
        ),
        migrations.CreateModel(
            name='GiaoDuc',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '5 Giáo Dục',
            },
        ),
        migrations.CreateModel(
            name='GioiTre',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '9 Giới Trẻ',
            },
        ),
        migrations.CreateModel(
            name='KinhTe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '2 Kinh Tế',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='img')),
                ('name', models.CharField(max_length=20)),
                ('content', models.TextField(null=True)),
                ('body', models.TextField(blank=True, null=True)),
                ('body1', models.TextField(blank=True, null=True)),
                ('body2', models.TextField(blank=True, null=True)),
                ('body3', models.TextField(blank=True, null=True)),
                ('slug', models.SlugField()),
            ],
            options={
                'verbose_name_plural': '0 Trang Chủ',
            },
        ),
        migrations.CreateModel(
            name='SucKhoe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '4 Sức Khỏe',
            },
        ),
        migrations.CreateModel(
            name='TheThao',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '8 Thể Thao',
            },
        ),
        migrations.CreateModel(
            name='ThoiSu',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': '1 Thời Sự',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('otp', models.CharField(max_length=6)),
                ('otp_expiry', models.DateTimeField()),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ThoiSuBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('thoisu', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.thoisu')),
            ],
            options={
                'verbose_name_plural': '1.1 Thời Sự Bài Viết',
            },
        ),
        migrations.CreateModel(
            name='TheThaoBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField()),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('thethao', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.thethao')),
            ],
            options={
                'verbose_name_plural': '8.1 Thể Thao Bài Viết',
            },
        ),
        migrations.CreateModel(
            name='SucKhoeBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('suckhoe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.suckhoe')),
            ],
            options={
                'verbose_name_plural': '4.1 Sức Khỏe Bài Viết',
            },
        ),
        migrations.CreateModel(
            name='KinhTeBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('kinhte', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.kinhte')),
            ],
            options={
                'verbose_name_plural': '2.1 Kinh Tế Bài Viết',
            },
        ),
        migrations.CreateModel(
            name='GioiTreBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField()),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('gioitre', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.gioitre')),
            ],
            options={
                'verbose_name_plural': '9.1 Giới Trẻ Bài Viết',
            },
        ),
        migrations.CreateModel(
            name='GiaoDucBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('giaoduc', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.giaoduc')),
            ],
            options={
                'verbose_name_plural': '5.1 Giáo Dục Bài Viết',
            },
        ),
        migrations.CreateModel(
            name='GiaiTriBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('giaitri', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.giaitri')),
            ],
            options={
                'verbose_name_plural': '7.1 Giải Trí Bài Viết',
            },
        ),
        migrations.CreateModel(
            name='DuLichBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('dulich', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.dulich')),
            ],
            options={
                'verbose_name_plural': '6.1 Du Lịch Bài Viết',
            },
        ),
        migrations.CreateModel(
            name='DoiSongBaiViet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titel', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='img')),
                ('body', models.TextField(blank=True, null=True)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('my_datetime', models.DateField()),
                ('slug', models.SlugField()),
                ('doisong', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.doisong')),
            ],
            options={
                'verbose_name_plural': '3.1 Đời Sống Bài Viết',
            },
        ),
    ]
