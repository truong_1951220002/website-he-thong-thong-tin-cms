from django.contrib import admin
from django.urls import path
from . import views 
from . import views_thoisu 
from . import views_kinhte
from . import views_doisong
from . import views_suckhoe
from . import views_giaoduc
from . import views_dulich
from . import views_giaitri
from . import views_thethao
from . import views_gioitre
from . import views_profile


urlpatterns = [
     path('', views.index, name='index'),

     path('search/', views.search, name="search"),
     # --------------------------------------- PHẦN Đăng ký đăng nhập đăng xuất  ---------------------------------------
     path('dangky/', views.dangky, name='dangky'),
     path('dangnhap/', views.dangnhap, name='dangnhap'),
     path('dangxuat/', views.dangxuat, name='dangxuat'),
     path('verify_otp/<int:user_id>/', views.verify_otp, name='verify_otp'),
# --------------------------------------- PHẦN menu  ---------------------------------------
     path('thoisu/', views_thoisu.thoisu, name="thoisu"),
     path('kinhte/', views_kinhte.kinhte, name="kinhte"),
     path('doisong/', views_doisong.doisong, name="doisong"),
     path('suckhoe/', views_suckhoe.suckhoe, name="suckhoe"),
     path('giaoduc/', views_giaoduc.giaoduc, name="giaoduc"),
     path('dulich/', views_dulich.dulich, name="dulich"),
     path('thethao/', views_thethao.thethao, name="thethao"),
     path('giaitri/', views_giaitri.giaitri, name="giaitri"),
     path('gioitre/', views_gioitre.gioitre, name="gioitre"),
# --------------------------------------- PHẦN SLUG  ---------------------------------------

     path('thoisu/<str:slug>', views_thoisu.detail, name='detail'),
     path('kinhte/<str:slug>', views_kinhte.detail, name='detail'),
     path('doisong/<str:slug>', views_doisong.detail, name='detail'),
     path('suckhoe/<str:slug>', views_suckhoe.detail, name='detail'),
     path('giaoduc/<str:slug>', views_giaoduc.detail, name='detail'),
     path('giaitri/<str:slug>', views_giaitri.detail, name='detail'),
     path('thethao/<str:slug>', views_thethao.detail, name='detail'),
     path('gioitre/<str:slug>', views_gioitre.detail, name='detail'),

# --------------------------------------- PHẦN SLUG  ---------------------------------------
   path('dulich/<str:slug>', views_dulich.show_detail, name='detail'),



# --------------------------------------- PHẦN PROFILE  ---------------------------------------
   path('profile/', views_profile.profile, name="profile"),
   path('doimatkhau/', views_profile.doimatkhau, name="doimatkhau"),
   path('save_profile/', views_profile.save_profile, name='save_profile'),
   path('luutrutin/', views_profile.luutrutin, name='luutrutin'),

] 
