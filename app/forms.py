from django import forms
from .models import ThoiSu, ThoiSuBaiViet
from .models import UserProfile
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User




class SignUpForm(UserCreationForm):
    email = forms.EmailField()
    profile_image = forms.ImageField(required=False)

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2', 'profile_image']
        first_name = forms.CharField(max_length=30, required=True, help_text='Required. Enter your first name.')
        last_name = forms.CharField(max_length=30, required=True, help_text='Required. Enter your last name.')
        class Meta:
            model = User
            fields = ['username', 'email', 'password1', 'password2', 'first_name', 'last_name', 'profile_image']
        
        error_messages = {
            'username': {
                'unique': ('Tên người dùng đã tồn tại. Vui lòng chọn một tên khác.'),
            },
        }
        
class UserProfileForm(forms.ModelForm):
    new_profile_image = forms.ImageField(required=False)

    class Meta:
        model = UserProfile
        fields = ['otp', 'otp_expiry', 'profile_image']

        error_messages = {
            'username': {
                'unique': ('Tên người dùng đã tồn tại. Vui lòng chọn một tên khác.'),
            },
        } 
class ThoiSuForm(forms.ModelForm):
    class Meta:
        model = ThoiSu
        fields = ['name']

class ThoiSuBaiVietForm(forms.ModelForm):
    class Meta:
        model = ThoiSuBaiViet
        fields = ['titel', 'name', 'image', 'thoisu', 'body', 'content', 'my_datetime', 'slug']
class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(widget=forms.PasswordInput, label="Mật khẩu hiện tại")
    new_password = forms.CharField(widget=forms.PasswordInput, label="Mật khẩu mới")
    confirm_password = forms.CharField(widget=forms.PasswordInput, label="Xác nhận mật khẩu mới")        
 

