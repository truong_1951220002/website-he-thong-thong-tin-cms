from django.shortcuts import render
from .models import *
from .models import DoiSong
from datetime import datetime
def doisong(request):
    current_time = datetime.now()
    doisong1_name = 'Người sống quanh ta'
    doisong2_name = 'Gia đình'
    doisong3_name = 'Cộng đồng'
    doisong4_name = 'Ẩm thực'
    doisong5_name = 'Một nửa thế giới'

    try:
        doisong1 = DoiSong.objects.get(name=doisong1_name)
    except DoiSong.DoesNotExist:
        doisong1 = None
    try:
        doisong2 = DoiSong.objects.get(name=doisong2_name)
    except DoiSong.DoesNotExist:
        doisong2 = None
    try:
        doisong3 = DoiSong.objects.get(name=doisong3_name)
    except DoiSong.DoesNotExist:
        doisong3 = None
    try:
        doisong4 = DoiSong.objects.get(name=doisong4_name)
    except DoiSong.DoesNotExist:
        doisong4 = None
    try:
        doisong5 = DoiSong.objects.get(name=doisong5_name)
    except DoiSong.DoesNotExist:
        doisong5 = None
    # Lọc bài viết dựa trên đối tượng thoisu
    doisongbaiviets1 = DoiSongBaiViet.objects.filter(doisong=doisong1)
    doisongbaiviets2 = DoiSongBaiViet.objects.filter(doisong=doisong2)
    doisongbaiviets3 = DoiSongBaiViet.objects.filter(doisong=doisong3)
    doisongbaiviets4 = DoiSongBaiViet.objects.filter(doisong=doisong4)
    doisongbaiviets5 = DoiSongBaiViet.objects.filter(doisong=doisong5)
    context = {
        'doisongbaiviets1': doisongbaiviets1[:4],
        'doisongbaiviets2': doisongbaiviets2[:8],
        'doisongbaiviets3': doisongbaiviets3[:7],
        'doisongbaiviets4': doisongbaiviets4[:7],
        'doisongbaiviets5': doisongbaiviets5[:7],
        'doisong1': doisong1_name,
        'doisong2': doisong2_name,
        'doisong3': doisong3_name,
        'doisong4': doisong4_name,
        'doisong5': doisong5_name,
        'current_time': current_time,
        'active' : 'doisong'
    }
    return render(request, 'doisong/doisong.html', context)

def detail(request, slug):
    current_time = datetime.now()
    kinhte1_name = 'Chính Sách - Phát Triển'
    dulich1_name = 'Khám phá'
    suckhoe2_name = 'Làm đẹp' 
    giaitri2_name = 'Truyền hình'
    giaitri3_name = 'Phim'
    suckhoe3_name = 'Giới tính'
  
    try:
        kinhte1 = KinhTe.objects.get(name=kinhte1_name)
    except KinhTe.DoesNotExist:
        kinhte1 = None  
     # ----------------------------------
    try:
        dulich1 = DuLich.objects.get(name=dulich1_name)
    except DuLich.DoesNotExist:
        dulich1 = None
    try:
        suckhoe2 = SucKhoe.objects.get(name=suckhoe2_name)
    except SucKhoe.DoesNotExist:
        suckhoe2 = None 
    try:
        giaitri2 = GiaiTri.objects.get(name=giaitri2_name)
    except GiaiTri.DoesNotExist:
        giaitri2 = None 
    try:
        giaitri3 = GiaiTri.objects.get(name=giaitri3_name)
    except GiaiTri.DoesNotExist:
        giaitri3 = None  
    try:
        suckhoe3 = SucKhoe.objects.get(name=suckhoe3_name)
    except SucKhoe.DoesNotExist:
        suckhoe3 = None   

# ----------------------------------
    if DoiSongBaiViet.objects.filter(slug=slug).exists():
        doisongbaiviets = DoiSongBaiViet.objects.filter(slug=slug).first()
    kinhtebaiviets1 = KinhTeBaiViet.objects.filter(kinhte=kinhte1)
    dulichbaiviets1 = DuLichBaiViet.objects.filter(dulich=dulich1)
    suckhoebaiviets2 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe2)
    giaitribaiviets2 = GiaiTriBaiViet.objects.filter(giaitri=giaitri2)
    giaitribaiviets3 = GiaiTriBaiViet.objects.filter(giaitri=giaitri3)
    suckhoebaiviets3 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe3)
    context= {'data': doisongbaiviets,
              'kinhtebaiviets1': kinhtebaiviets1[:2],
              'dulichbaiviets1': dulichbaiviets1[:2], 
              'suckhoebaiviets2': suckhoebaiviets2[:4],
               'suckhoebaiviets3': suckhoebaiviets3[:6],
              'giaitribaiviets2': giaitribaiviets2[:4],
              'giaitribaiviets3': giaitribaiviets3[:5],
              'active' : 'doisong',
              'current_time': current_time,
              }  

    return render(request, 'app/detail.html',context)