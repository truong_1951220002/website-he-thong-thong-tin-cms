from django.shortcuts import render
from .models import *
from .models import SucKhoe
from datetime import datetime

def suckhoe(request):
    current_time = datetime.now()
    suckhoe1_name = 'Khỏe đẹp mỗi ngày'
    suckhoe2_name = 'Làm đẹp'
    suckhoe3_name = 'Giới tính'
    suckhoe4_name = 'Sống đẹp'
    
    try:
        suckhoe1 = SucKhoe.objects.get(name=suckhoe1_name)
    except SucKhoe.DoesNotExist:
        suckhoe1 = None
    try:
        suckhoe2 = SucKhoe.objects.get(name=suckhoe2_name)
    except SucKhoe.DoesNotExist:
        suckhoe2 = None
    try:
        suckhoe3 = SucKhoe.objects.get(name=suckhoe3_name)
    except SucKhoe.DoesNotExist:
        suckhoe3 = None
    try:
        suckhoe4 = SucKhoe.objects.get(name=suckhoe4_name)
    except SucKhoe.DoesNotExist:
        suckhoe4 = None
    # Lọc bài viết dựa trên đối tượng thoisu
    suckhoebaiviets1 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe1)
    suckhoebaiviets2 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe2)
    suckhoebaiviets3 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe3)
    suckhoebaiviets4 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe4)
    context = {
        'suckhoebaiviets1': suckhoebaiviets1[:6],
        'suckhoebaiviets2': suckhoebaiviets2[:8],
        'suckhoebaiviets3': suckhoebaiviets3[:6],
        'suckhoebaiviets4': suckhoebaiviets4[:6],
        'suckhoe1': suckhoe1_name,
        'suckhoe2': suckhoe2_name,
        'suckhoe3': suckhoe3_name,
        'suckhoe4': suckhoe4_name,
        'active' : 'suckhoe',
        'current_time': current_time,
        
    }
    return render(request, 'suckhoe/suckhoe.html', context)

def detail(request, slug):
    current_time = datetime.now()
    thethao1_name = 'Bóng đá Việt Nam'
    giaitri1_name = 'Kết nối'
    thethao2_name = 'Bóng đá Quốc Tế'
    giaitri3_name = 'Phim'
    doisong3_name = 'Cộng đồng'
    thethao3_name = 'Thể thao khác'
    try:
        thethao1 = TheThao.objects.get(name=thethao1_name)
    except TheThao.DoesNotExist:
        thethao1 = None
    try:
        giaitri1 = GiaiTri.objects.get(name=giaitri1_name)
    except GiaiTri.DoesNotExist:
        giaitri1 = None   
    try:
        thethao2 = TheThao.objects.get(name=thethao2_name)
    except TheThao.DoesNotExist:
        thethao2 = None
    try:
        giaitri3 = GiaiTri.objects.get(name=giaitri3_name)
    except GiaiTri.DoesNotExist:
        giaitri3 = None   
    try:
        doisong3 = DoiSong.objects.get(name=doisong3_name)
    except DoiSong.DoesNotExist:
        doisong3 = None 
    try:
        thethao3 = TheThao.objects.get(name=thethao3_name)
    except TheThao.DoesNotExist:
        thethao3 = None
    if SucKhoeBaiViet.objects.filter(slug=slug).exists():
        suckhoebaiviets = SucKhoeBaiViet.objects.filter(slug=slug).first()
        thethaobaiviets1 = TheThaoBaiViet.objects.filter(thethao=thethao1)
        giaitribaiviets1 = GiaiTriBaiViet.objects.filter(giaitri=giaitri1)
        thethaobaiviets2 = TheThaoBaiViet.objects.filter(thethao=thethao2)
        doisongbaiviets3 = DoiSongBaiViet.objects.filter(doisong=doisong3)
        thethaobaiviets3 = TheThaoBaiViet.objects.filter(thethao=thethao3)
        giaitribaiviets3 = GiaiTriBaiViet.objects.filter(giaitri=giaitri3)
        context= {'data': suckhoebaiviets,
                  'thethaobaiviets1': thethaobaiviets1[:2],
                  'giaitribaiviets1': giaitribaiviets1[:2],
                  'thethaobaiviets2': thethaobaiviets2[:4],
                  'thethaobaiviets3': thethaobaiviets3[:6],
                  'giaitribaiviets3': giaitribaiviets3[:5],
                  'doisongbaiviets3': doisongbaiviets3[:7],
                   'active' : 'suckhoe',
                   'current_time': current_time,
                  }  

    return render(request, 'app/detail.html',context)