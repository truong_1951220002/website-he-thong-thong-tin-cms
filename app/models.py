from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.contrib.auth.models import User



# class CreateUserForm(UserCreationForm):
#     class Meta:
#         model = User
#         fields = ['username', 'email', 'password1', 'password2','profile_image']
#         error_messages = {
#             'username': {
#                 'unique': ('Tên đăng nhập đã tồn tại. Vui lòng chọn tên đăng nhập khác.'),
#             },
#         }

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    otp = models.CharField(max_length=6)
    otp_expiry = models.DateTimeField(null=True, blank=True)
    profile_image = models.ImageField(upload_to='images/', blank=True, null=True)

    def __str__(self):
        return self.user.username 
    


  #  --------------------------------------------- Phan thoi su ---------------------------------------------
class ThoiSu(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "1 Thời Sự"
    def __str__(self):
        return self.name
class ThoiSuBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    thoisu = models.ForeignKey(ThoiSu, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)  # Sử dụng models.TextField() cho trường nội dung 
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "1.1 Thời Sự Bài Viết"
    def __str__(self):
        return self.titel
    



 #  --------------------------------------------- Phần Kinh Tế ---------------------------------------------
class KinhTe(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "2 Kinh Tế"
    def __str__(self):
        return self.name
class KinhTeBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    kinhte = models.ForeignKey(KinhTe, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)  # Sử dụng models.TextField() cho trường nội dung 
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "2.1 Kinh Tế Bài Viết"
    def __str__(self):
        return self.titel
 #  --------------------------------------------- Phần đời sống ---------------------------------------------
class DoiSong(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "3 Đời Sống"
    def __str__(self):
        return self.name
class DoiSongBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    doisong = models.ForeignKey(DoiSong, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)  # Sử dụng models.TextField() cho trường nội dung 
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "3.1 Đời Sống Bài Viết"
    def __str__(self):
        return self.titel
    #  --------------------------------------------- Sức khỏe ---------------------------------------------
class SucKhoe(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "4 Sức Khỏe"
    def __str__(self):
        return self.name
class SucKhoeBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    suckhoe = models.ForeignKey(SucKhoe, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)  # Sử dụng models.TextField() cho trường nội dung 
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "4.1 Sức Khỏe Bài Viết"
    def __str__(self):
        return self.titel
 #  --------------------------------------------- Giáo dục ---------------------------------------------
class GiaoDuc(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "5 Giáo Dục"
    def __str__(self):
        return self.name
class GiaoDucBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    giaoduc = models.ForeignKey(GiaoDuc, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)  # Sử dụng models.TextField() cho trường nội dung 
    # video_file = models.FileField(upload_to='videos/')  # Lưu video vào thư mục 'media/videos/'
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "5.1 Giáo Dục Bài Viết"
    def __str__(self):
        return self.titel
 #  --------------------------------------------- Du lịch ---------------------------------------------
class DuLich(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "6 Du Lịch"
    def __str__(self):
        return self.name
class DuLichBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    dulich = models.ForeignKey(DuLich, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)  # Sử dụng models.TextField() cho trường nội dung 
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "6.1 Du Lịch Bài Viết"
    def __str__(self):
        return self.titel
 #  --------------------------------------------- Giải Trí ---------------------------------------------

class GiaiTri(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "7 Giải Trí"
    def __str__(self):
        return self.name
    
class GiaiTriBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    giaitri = models.ForeignKey(GiaiTri, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)  # Sử dụng models.TextField() cho trường nội dung 
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "7.1 Giải Trí Bài Viết"
    def __str__(self):
        return self.titel
 #  --------------------------------------------- Thể thao---------------------------------------------

class TheThao(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "8 Thể Thao"
    def __str__(self):
        return self.name
    
    
class TheThaoBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    thethao = models.ForeignKey(TheThao, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField()  # Sử dụng models.TextField() cho trường nội dung 
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "8.1 Thể Thao Bài Viết"
    def __str__(self):
        return self.titel
#  --------------------------------------------- Thể thao---------------------------------------------
class GioiTre(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name_plural = "9 Giới Trẻ"
    def __str__(self):
        return self.name
class GioiTreBaiViet(models.Model):
    titel = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='img')
    gioitre = models.ForeignKey(GioiTre, on_delete=models.CASCADE) # Thay thế bằng ForeignKey nếu bạn muốn tạo mối quan hệ với mô hình danh mục
    body = models.TextField(blank=True, null=True)
    content = RichTextUploadingField()  # Sử dụng models.TextField() cho trường nội dung 
    my_datetime = models.DateField()
    slug = models.SlugField()
    class Meta:
        verbose_name_plural = "9.1 Giới Trẻ Bài Viết"
    def __str__(self):
        return self.titel