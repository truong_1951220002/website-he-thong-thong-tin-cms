from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from ckeditor_uploader.widgets import CKEditorUploadingWidget

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
class UserProfileAdmin(UserAdmin):
    inlines = (UserProfileInline, )
admin.site.unregister(User)
admin.site.register(User, UserProfileAdmin)

#  --------------------------------------------- Phan thoi su ---------------------------------------------
class ThoiSuAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class ThoiSuBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','thoisu', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'thoisu')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','thoisu')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}

# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(ThoiSu, ThoiSuAdmin)
admin.site.register(ThoiSuBaiViet, ThoiSuBaiVietAdmin)
#  --------------------------------------------- Phần Kinh Tế ---------------------------------------------
class KinhTeAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class KinhTeBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','kinhte', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'kinhte')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','kinhte')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}

# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(KinhTe, KinhTeAdmin)
admin.site.register(KinhTeBaiViet, KinhTeBaiVietAdmin)
 #  --------------------------------------------- Phần đời sống -----------------------------------------
class DoiSongAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class DoiSongBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','doisong', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'doisong')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','doisong')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}

# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(DoiSong, DoiSongAdmin)
admin.site.register(DoiSongBaiViet, DoiSongBaiVietAdmin)
 #  --------------------------------------------- Sức khỏe ---------------------------------------------
class SucKhoeAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class SucKhoeBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','suckhoe', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'suckhoe')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','suckhoe')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}

# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(SucKhoe, SucKhoeAdmin)
admin.site.register(SucKhoeBaiViet, SucKhoeBaiVietAdmin)
 #  --------------------------------------------- Giáo dục ---------------------------------------------
class GiaoDucAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class GiaoDucBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','giaoduc', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'giaoduc')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','giaoduc')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}

# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(GiaoDuc, GiaoDucAdmin)
admin.site.register(GiaoDucBaiViet, GiaoDucBaiVietAdmin)
 #  --------------------------------------------- Du lịch ---------------------------------------------
class DuLichAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class DuLichBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','dulich', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'dulich')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','dulich')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}

# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(DuLich, DuLichAdmin)
admin.site.register(DuLichBaiViet, DuLichBaiVietAdmin)
 #  --------------------------------------------- Giải Trí ---------------------------------------------
class GiaiTriAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class GiaiTriBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','giaitri', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'giaitri')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','giaitri')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}

# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(GiaiTri, GiaiTriAdmin)
admin.site.register(GiaiTriBaiViet, GiaiTriBaiVietAdmin)
 #  --------------------------------------------- Thể thao---------------------------------------------
class TheThaoAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class TheThaoBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','thethao', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'thethao')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','thethao')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}
  
# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(TheThao, TheThaoAdmin)
admin.site.register(TheThaoBaiViet, TheThaoBaiVietAdmin)
 #  --------------------------------------------- Thể thao---------------------------------------------
class GioiTreAdmin(admin.ModelAdmin):
    list_display = ('name',)  # Hiển thị trường name trong danh sách
    list_filter = ('name',)   # Tạo bộ lọc theo trường name
    search_fields = ('name',) # Tạo ô tìm kiếm theo trường name

class GioiTreBaiVietAdmin(admin.ModelAdmin):
    list_display = ('titel','gioitre', 'name', 'my_datetime')  # Hiển thị trường titel, name, và my_datetime trong danh sách
    list_filter = ('my_datetime', 'gioitre')  # Tạo bộ lọc theo trường my_datetime và category
    search_fields = ('titel', 'name','gioitre')  # Tạo ô tìm kiếm theo trường titel và name
    prepopulated_fields ={'slug': ('titel',)}
   
# Đăng ký các lớp admin.ModelAdmin với các mô hình tương ứng
admin.site.register(GioiTre, GioiTreAdmin)
admin.site.register(GioiTreBaiViet, GioiTreBaiVietAdmin)