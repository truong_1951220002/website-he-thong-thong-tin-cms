from django.shortcuts import render
from .models import *
from .models import GiaiTri
from datetime import datetime
def giaitri(request):
    current_time = datetime.now()
    context= {}
    giaitri1_name = 'Xem - Nghe'
    giaitri2_name = 'Truyền hình'
    giaitri3_name = 'Phim'
    giaitri4_name = 'Đời nghệ sĩ'

    try:
        giaitri1 = GiaiTri.objects.get(name=giaitri1_name)
    except GiaiTri.DoesNotExist:
        giaitri1 = None
    try:
        giaitri2 = GiaiTri.objects.get(name=giaitri2_name)
    except GiaiTri.DoesNotExist:
        giaitri2 = None
    try:
        giaitri3 = GiaiTri.objects.get(name=giaitri3_name)
    except GiaiTri.DoesNotExist:
        giaitri3 = None
    try:
        giaitri4 = GiaiTri.objects.get(name=giaitri4_name)
    except GiaiTri.DoesNotExist:
        giaitri4 = None
    
    
    # Lọc bài viết dựa trên đối tượng thoisu
    giaitribaiviets1 = GiaiTriBaiViet.objects.filter(giaitri=giaitri1)
    giaitribaiviets2 = GiaiTriBaiViet.objects.filter(giaitri=giaitri2)
    giaitribaiviets3 = GiaiTriBaiViet.objects.filter(giaitri=giaitri3)
    giaitribaiviets4 = GiaiTriBaiViet.objects.filter(giaitri=giaitri4)
    
    context = {
        'giaitribaiviets1': giaitribaiviets1[:4],
        'giaitribaiviets2': giaitribaiviets2[:6],
        'giaitribaiviets3': giaitribaiviets3[:5],
        'giaitribaiviets4': giaitribaiviets4[:5],
        'giaitri1': giaitri1_name,
        'giaitri2': giaitri2_name,
        'giaitri3': giaitri3_name,
        'giaitri4': giaitri4_name,
        'active' : 'giaitri',
        'current_time': current_time,
    }
    return render(request, 'giaitri/giaitri.html', context)

def detail(request, slug):
    current_time = datetime.now()
    thethao1_name = 'Bóng đá Việt Nam'
    gioitre1_name = 'Kết Nối'
    thethao2_name = 'Bóng đá Quốc Tế'
    gioitre3_name = 'Thế Giới Mạng'
    giaoduc3_name = 'Tuyển Sinh'
    gioitre2_name = 'Khởi nghiệp'
    thethao4_name = 'Bóng rổ'
    try:
        thethao1 = TheThao.objects.get(name=thethao1_name)
    except TheThao.DoesNotExist:
        thethao1 = None
    try:
        gioitre1 = GioiTre.objects.get(name=gioitre1_name)
    except GioiTre.DoesNotExist:
        gioitre1 = None
    try:
        thethao2 = TheThao.objects.get(name=thethao2_name)
    except TheThao.DoesNotExist:
        thethao2 = None
    try:
        gioitre3 = GioiTre.objects.get(name=gioitre3_name)
    except GioiTre.DoesNotExist:
        gioitre3 = None
    try:
        giaoduc3 = GiaoDuc.objects.get(name=giaoduc3_name)
    except GiaoDuc.DoesNotExist:
        giaoduc3 = None
    try:
        gioitre2 = GioiTre.objects.get(name=gioitre2_name)
    except GioiTre.DoesNotExist:
        gioitre2 = None
    try:
        thethao4 = TheThao.objects.get(name=thethao4_name)
    except TheThao.DoesNotExist:
        thethao4 = None
    if GiaiTriBaiViet.objects.filter(slug=slug).exists():
        giaitriaiviets = GiaiTriBaiViet.objects.filter(slug=slug).first()
        thethaobaiviets1 = TheThaoBaiViet.objects.filter(thethao=thethao1)
        gioitrebaiviets1 = GioiTreBaiViet.objects.filter(gioitre=gioitre1)
        thethaobaiviets2 = TheThaoBaiViet.objects.filter(thethao=thethao2)
        gioitrebaiviets2 = GioiTreBaiViet.objects.filter(gioitre=gioitre2)
        gioitrebaiviets3 = GioiTreBaiViet.objects.filter(gioitre=gioitre3)
        thethaobaiviets4 = TheThaoBaiViet.objects.filter(thethao=thethao4)
        giaoducbaiviets3 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc3)
        context= {'data': giaitriaiviets,
                  'thethaobaiviets1': thethaobaiviets1[:2],
                  'gioitrebaiviets1': gioitrebaiviets1[:2],
                  'thethaobaiviets2': thethaobaiviets2[:4],
                   'gioitrebaiviets3': gioitrebaiviets3[:5],
                   'giaoducbaiviets3': giaoducbaiviets3[:4],
                   'thethaobaiviets4': thethaobaiviets4[:6],
                   'gioitrebaiviets2': gioitrebaiviets2[:6],
                   'active' : 'giaitri',
                   'current_time': current_time,
                  
                  }  

    return render(request, 'app/detail.html',context)
