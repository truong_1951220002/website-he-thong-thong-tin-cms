from django.shortcuts import render, get_object_or_404
from .models import *
from django.urls import reverse

from django.http import JsonResponse
from datetime import datetime
def thoisu(request):
    current_time = datetime.now()
    thoisu1_name = 'Chính trị'
    thoisu2_name = 'Pháp luật'
    thoisu3_name = 'Dân sinh'
    thoisu4_name = 'Quốc phòng'
    thoisu5_name = 'Quyền Được Biết'
    try:
        thoisu1 = ThoiSu.objects.get(name=thoisu1_name)
    except ThoiSu.DoesNotExist:
        thoisu1 = None
    try:
        thoisu2 = ThoiSu.objects.get(name=thoisu2_name)
    except ThoiSu.DoesNotExist:
        thoisu2 = None
    try:
        thoisu3 = ThoiSu.objects.get(name=thoisu3_name)
    except ThoiSu.DoesNotExist:
        thoisu3 = None 
    try:
        thoisu4 = ThoiSu.objects.get(name=thoisu4_name)
    except ThoiSu.DoesNotExist:
        thoisu4 = None 
    try:
        thoisu5 = ThoiSu.objects.get(name=thoisu5_name)
    except ThoiSu.DoesNotExist:
        thoisu5 = None   
    # Lọc bài viết dựa trên đối tượng thoisu
    thoisubaiviets1 = ThoiSuBaiViet.objects.filter(thoisu=thoisu1)
    thoisubaiviets2 = ThoiSuBaiViet.objects.filter(thoisu=thoisu2)
    thoisubaiviets3 = ThoiSuBaiViet.objects.filter(thoisu=thoisu3)
    thoisubaiviets4 = ThoiSuBaiViet.objects.filter(thoisu=thoisu4)
    thoisubaiviets5 = ThoiSuBaiViet.objects.filter(thoisu=thoisu5)
    context = {
        'thoisubaiviets1': thoisubaiviets1[:4],
        'thoisubaiviets2': thoisubaiviets2[:6],
        'thoisubaiviets3': thoisubaiviets3[:6],
        'thoisubaiviets4': thoisubaiviets4[:6],
        'thoisubaiviets5': thoisubaiviets5[:6],
        'thoisu1': thoisu1_name,
        'thoisu2': thoisu2_name,
        'thoisu3': thoisu3_name,
        'thoisu4': thoisu4_name,
        'thoisu5': thoisu5_name,
        'current_time': current_time,
        'active' : 'thoisu'
    }
    return render(request, 'thoisu/thoisu.html', context)

def detail(request, slug):
    current_time = datetime.now()
    kinhte1_name = 'Chính Sách - Phát Triển'
    dulich2_name = 'Câu chuyện du lịch'
    doisong2_name = 'Gia đình'
    giaoduc1_name = 'Nhà Trường'
    giaoduc3_name = 'Tuyển Sinh'
    doisong3_name = 'Cộng đồng'
    giaoduc2_name = 'Tuyển Sinh'

  
    
    try:
        kinhte1 = KinhTe.objects.get(name=kinhte1_name)
    except KinhTe.DoesNotExist:
        kinhte1 = None  
    
    try:
        doisong2 = DoiSong.objects.get(name=doisong2_name)
    except DoiSong.DoesNotExist:
        doisong2 = None
    try:
        giaoduc1 = GiaoDuc.objects.get(name=giaoduc1_name)
    except GiaoDuc.DoesNotExist:
        giaoduc1 = None
    try:
        dulich2 = DuLich.objects.get(name=dulich2_name)
    except DuLich.DoesNotExist:
        dulich2 = None
    try:
        giaoduc3 = GiaoDuc.objects.get(name=giaoduc3_name)
    except GiaoDuc.DoesNotExist:
        giaoduc3 = None
    try:
        doisong3 = DoiSong.objects.get(name=doisong3_name)
    except DoiSong.DoesNotExist:
        doisong3 = None
    try:
        giaoduc2 = GiaoDuc.objects.get(name=giaoduc2_name)
    except GiaoDuc.DoesNotExist:
        giaoduc2 = None
# -------------------------------------------------- 

   
    if ThoiSuBaiViet.objects.filter(slug=slug).exists():
        thoisubaiviets = ThoiSuBaiViet.objects.filter(slug=slug).first()
   
    giaoducbaiviets3 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc3)
    kinhtebaiviets1 = KinhTeBaiViet.objects.filter(kinhte=kinhte1)
    dulichbaiviets2 = DuLichBaiViet.objects.filter(dulich=dulich2)
    doisongbaiviets2 = DoiSongBaiViet.objects.filter(doisong=doisong2)
    doisongbaiviets3 = DoiSongBaiViet.objects.filter(doisong=doisong3) 
    giaoducbaiviets1 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc1)
    giaoducbaiviets2 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc2)

    
    context= {
               'data': thoisubaiviets,
               'giaoducbaiviets2': giaoducbaiviets2[:6],
                'kinhtebaiviets1': kinhtebaiviets1[:2],
                'giaoducbaiviets3': giaoducbaiviets3[:4],
                'doisongbaiviets2': doisongbaiviets2[:4],
                'giaoducbaiviets1': giaoducbaiviets1[:2],
                'dulichbaiviets2': dulichbaiviets2[:4],
                'doisongbaiviets3': doisongbaiviets3[:7],
                'active' : 'thoisu',
'current_time': current_time,
               
                  
                  }  
    return render(request, 'app/detail.html',context)

    

def luutrutin(request):
    # Lấy danh sách bài viết từ cơ sở dữ liệu
    thoisu1 = get_object_or_404(ThoiSu, name='Thoi Su 1')
    thoisu2 = get_object_or_404(ThoiSu, name='Thoi Su 2')
    thoisu3 = get_object_or_404(ThoiSu, name='Thoi Su 3')
    thoisu4 = get_object_or_404(ThoiSu, name='Thoi Su 4')
    thoisu5 = get_object_or_404(ThoiSu, name='Thoi Su 5')

    thoisubaiviets1 = ThoiSuBaiViet.objects.filter(thoisu=thoisu1)
    thoisubaiviets2 = ThoiSuBaiViet.objects.filter(thoisu=thoisu2)
    thoisubaiviets3 = ThoiSuBaiViet.objects.filter(thoisu=thoisu3)
    thoisubaiviets4 = ThoiSuBaiViet.objects.filter(thoisu=thoisu4)
    thoisubaiviets5 = ThoiSuBaiViet.objects.filter(thoisu=thoisu5)

    # Truyền danh sách bài viết đến template
    context = {
        'thoisubaiviets1': thoisubaiviets1,
        'thoisubaiviets2': thoisubaiviets2,
        'thoisubaiviets3': thoisubaiviets3,
        'thoisubaiviets4': thoisubaiviets4,
        'thoisubaiviets5': thoisubaiviets5,
    }

    return render(request, 'luutrutin.html', context)

def luutrutin(request):
    if request.method == 'POST' and request.is_ajax():
        # Lấy dữ liệu từ POST request
        slug = request.POST.get('slug', '')
        # Thêm các trường dữ liệu khác cần thiết từ form

        # Xác định bài viết cần lưu thông tin
        thoisu_baiviet = ThoiSuBaiViet.objects.get(slug=slug)

        # Thực hiện cập nhật dữ liệu bài viết
        # Ví dụ: thoisu_baiviet.field_name = request.POST.get('field_name')
        # thoisu_baiviet.save()

        # Trả về JsonResponse để thông báo cho client rằng lưu thành công
        return JsonResponse({'message': 'Lưu thành công'})

    # Nếu không phải là phương thức POST hoặc không phải là AJAX request, trả về lỗi
    return JsonResponse({'message': 'Invalid request'}, status=400)