from django.shortcuts import render
from .models import *
from .models import DuLich
from datetime import datetime
def dulich(request):
    current_time = datetime.now()
    context= {}
    dulich1_name = 'Khám phá'
    dulich2_name = 'Câu chuyện du lịch'
    dulich3_name = 'Bất động sản du lịch'
    dulich4_name = 'Tin tức- Sự kiện'
    dulich5_name = 'Chơi gì, ăn đâu, đi thế nào?'

    try:
        dulich1 = DuLich.objects.get(name=dulich1_name)
    except DuLich.DoesNotExist:
        dulich1 = None
    try:
        dulich2 = DuLich.objects.get(name=dulich2_name)
    except DuLich.DoesNotExist:
        dulich2 = None
    try:
        dulich3 = DuLich.objects.get(name=dulich3_name)
    except DuLich.DoesNotExist:
        dulich3 = None
    try:
        dulich4 = DuLich.objects.get(name=dulich4_name)
    except DuLich.DoesNotExist:
        dulich4 = None
    try:
        dulich5 = DuLich.objects.get(name=dulich5_name)
    except DuLich.DoesNotExist:
        dulich5 = None

     
    # Lọc bài viết dựa trên đối tượng thoisu
    dulichbaiviets1 = DuLichBaiViet.objects.filter(dulich=dulich1)
    dulichbaiviets2 = DuLichBaiViet.objects.filter(dulich=dulich2)
    dulichbaiviets3 = DuLichBaiViet.objects.filter(dulich=dulich3)
    dulichbaiviets4 = DuLichBaiViet.objects.filter(dulich=dulich4)
    dulichbaiviets5 = DuLichBaiViet.objects.filter(dulich=dulich5)
    
    context = {
        'dulichbaiviets1': dulichbaiviets1[:4],
        'dulichbaiviets2': dulichbaiviets2[:6],
        'dulichbaiviets3': dulichbaiviets3[:5],
        'dulichbaiviets4': dulichbaiviets4[:5],
        'dulichbaiviets5': dulichbaiviets5[:5],
        'dulich1': dulich1_name,
        'dulich2': dulich2_name,
        'dulich3': dulich3_name,
        'dulich4': dulich4_name,
        'dulich5': dulich5_name,
        'current_time': current_time,
        'active' : 'dulich'
        
    }
    return render(request, 'dulich/dulich.html', context)

# def detail(request, slug):
#     context = {}
#     if DuLichBaiViet.objects.filter(slug=slug).exists():
#         dulichaiviets = DuLichBaiViet.objects.filter(slug=slug).first()
#         context= {'data': dulichaiviets}  

#     return render(request, 'app/detail.html',context)

def show_detail(request, slug):
    current_time = datetime.now()
    thoisu1_name = 'Chính trị'
    thoisu2_name = 'Pháp luật'
    doisong2_name = 'Gia đình'
    doisong3_name = 'Cộng đồng'
    kinhte1_name = 'Chính Sách - Phát Triển'
    kinhte2_name = 'Ngân Hàng'
    kinhte3_name = 'Chứng Khoán'
    
    try:
        thoisu1 = ThoiSu.objects.get(name=thoisu1_name)
    except ThoiSu.DoesNotExist:
        thoisu1 = None
    try:
        thoisu2 = ThoiSu.objects.get(name=thoisu2_name)
    except ThoiSu.DoesNotExist:
        thoisu2 = None          
    try:
        doisong2 = DoiSong.objects.get(name=doisong2_name)
    except DoiSong.DoesNotExist:
        doisong2 = None   
    try:
        doisong3 = DoiSong.objects.get(name=doisong3_name)
    except DoiSong.DoesNotExist:
        doisong3 = None     
    try:
        kinhte1 = KinhTe.objects.get(name=kinhte1_name)
    except KinhTe.DoesNotExist:
        kinhte1 = None   
    try:
        kinhte2 = KinhTe.objects.get(name=kinhte2_name)
    except KinhTe.DoesNotExist:
        kinhte2 = None
    try:
        kinhte3 = KinhTe.objects.get(name=kinhte3_name)
    except KinhTe.DoesNotExist:
        kinhte3 = None    
    thoisubaiviets1 = ThoiSuBaiViet.objects.filter(thoisu=thoisu1)
    thoisubaiviets2 = ThoiSuBaiViet.objects.filter(thoisu=thoisu2)
    doisongbaiviets2 = DoiSongBaiViet.objects.filter(doisong=doisong2)
    doisongbaiviets3 = DoiSongBaiViet.objects.filter(doisong=doisong3)
    kinhtebaiviets1 = KinhTeBaiViet.objects.filter(kinhte=kinhte1)
    kinhtebaiviets2 = KinhTeBaiViet.objects.filter(kinhte=kinhte2)
    kinhtebaiviets3 = KinhTeBaiViet.objects.filter(kinhte=kinhte3)
    
    if DuLichBaiViet.objects.filter(slug=slug).exists():
        dulichbaiviets = DuLichBaiViet.objects.filter(slug=slug).first()
        doisongbaiviets2 = DoiSongBaiViet.objects.filter(doisong=doisong2)
        context= {'detail': dulichbaiviets,
                  'thoisubaiviets1': thoisubaiviets1[:2],
                  'kinhtebaiviets1': kinhtebaiviets1[:2],
                  'kinhtebaiviets2': kinhtebaiviets2[:3],
                  'kinhtebaiviets3': kinhtebaiviets3[:5],
                  'thoisubaiviets2': thoisubaiviets2[:4],
                  'doisongbaiviets2': doisongbaiviets2[:4],
                  'doisongbaiviets3': doisongbaiviets3[:5],
                  'thoisu1': thoisu1_name,
                   'active' : 'dulich',
                   'current_time': current_time,
                  } 

    

    return render(request, 'app/show_detail.html',context)
