from django.shortcuts import render
from .models import *
from .models import KinhTe
from datetime import datetime
def kinhte(request):
    current_time = datetime.now()
    kinhte1_name = 'Chính sách - Phát triển'
    kinhte2_name = 'Ngân hàng'
    kinhte3_name = 'Chứng khoán'
    kinhte4_name = 'Doanh nghiệp'
    kinhte5_name = 'Làm giàu'

    try:
        kinhte1 = KinhTe.objects.get(name=kinhte1_name)
    except KinhTe.DoesNotExist:
        kinhte1 = None
    try:
        kinhte2 = KinhTe.objects.get(name=kinhte2_name)
    except KinhTe.DoesNotExist:
        kinhte2 = None
    try:
        kinhte3 = KinhTe.objects.get(name=kinhte3_name)
    except KinhTe.DoesNotExist:
        kinhte3 = None
    try:
        kinhte4 = KinhTe.objects.get(name=kinhte4_name)
    except KinhTe.DoesNotExist:
        kinhte4 = None
    try:
        kinhte5 = KinhTe.objects.get(name=kinhte5_name)
    except KinhTe.DoesNotExist:
        kinhte5 = None


        
    # Lọc bài viết dựa trên đối tượng thoisu
    kinhtebaiviets1 = KinhTeBaiViet.objects.filter(kinhte=kinhte1)
    kinhtebaiviets2 = KinhTeBaiViet.objects.filter(kinhte=kinhte2)
    kinhtebaiviets3 = KinhTeBaiViet.objects.filter(kinhte=kinhte3)
    kinhtebaiviets4 = KinhTeBaiViet.objects.filter(kinhte=kinhte4)
    kinhtebaiviets5 = KinhTeBaiViet.objects.filter(kinhte=kinhte5)
    context = {
        'kinhtebaiviets1': kinhtebaiviets1[:4],
        'kinhtebaiviets2': kinhtebaiviets2[:14],
        'kinhtebaiviets3': kinhtebaiviets3[:8],
        'kinhtebaiviets4': kinhtebaiviets4[:8],
        'kinhtebaiviets5': kinhtebaiviets5[:6],
        'kinhte1': kinhte1_name,
        'kinhte2': kinhte2_name,
        'kinhte3': kinhte3_name,
        'kinhte4': kinhte4_name,
        'kinhte5': kinhte5_name,
        'current_time': current_time,
        'active' : 'kinhte'
        
    }
    return render(request, 'kinhte/kinhte.html', context)

def detail(request, slug):
    current_time = datetime.now()
    thoisu1_name = 'Chính trị'
    giaoduc1_name = 'Nhà Trường'
    giaitri2_name = 'Truyền hình'
    suckhoe3_name = 'Giới tính'
    giaoduc4_name = 'Du học'
    doisong3_name = 'Cộng đồng'
    try:
        thoisu1 = ThoiSu.objects.get(name=thoisu1_name)
    except ThoiSu.DoesNotExist:
        thoisu1 = None
    try:
        giaoduc1 = GiaoDuc.objects.get(name=giaoduc1_name)
    except GiaoDuc.DoesNotExist:
        giaoduc1 = None
    try:
        giaitri2 = GiaiTri.objects.get(name=giaitri2_name)
    except GiaiTri.DoesNotExist:
        giaitri2 = None
    try:
        suckhoe3 = SucKhoe.objects.get(name=suckhoe3_name)
    except SucKhoe.DoesNotExist:
        suckhoe3 = None
    try:
        giaoduc4 = GiaoDuc.objects.get(name=giaoduc4_name)
    except GiaoDuc.DoesNotExist:
        giaoduc4 = None
    try:
        doisong3 = DoiSong.objects.get(name=doisong3_name)
    except DoiSong.DoesNotExist:
        doisong3 = None
    if KinhTeBaiViet.objects.filter(slug=slug).exists():
        kinhtebaiviets = KinhTeBaiViet.objects.filter(slug=slug).first()
    thoisubaiviets1 = ThoiSuBaiViet.objects.filter(thoisu=thoisu1)
    giaoducbaiviets1 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc1)
    giaitribaiviets2 = GiaiTriBaiViet.objects.filter(giaitri=giaitri2)
    suckhoebaiviets3 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe3)
    giaoducbaiviets4 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc4)
    doisongbaiviets3 = DoiSongBaiViet.objects.filter(doisong=doisong3)

    


    
    context= { 
                 'data': kinhtebaiviets,
                'thoisubaiviets1': thoisubaiviets1[:2],
                'giaoducbaiviets1': giaoducbaiviets1[:2],
                'giaitribaiviets2': giaitribaiviets2[:4],
                'suckhoebaiviets3': suckhoebaiviets3[:6],
                'giaoducbaiviets4': giaoducbaiviets4[:6],
                'doisongbaiviets3': doisongbaiviets3[:7],
                'active' : 'kinhte',
                 'current_time': current_time,
                  }  

    return render(request, 'app/detail.html',context)
