from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import *
from .forms import UserProfileForm
from django.contrib.auth import authenticate,login,logout
from django.core.mail import send_mail
from django.http import Http404
from django.contrib import messages
from .models import UserProfile
import random
from datetime import datetime, timedelta
from .forms import SignUpForm
from datetime import datetime
def index(request):
    current_time = datetime.now()
# --------------------------------------------------------------------------------
    thoisu1_name = 'Chính trị'
    thoisu2_name = 'Pháp luật'
    thoisu3_name = 'Dân sinh'
    thoisu5_name = 'Quyền Được Biết'

    kinhte1_name = 'Chính Sách - Phát Triển'
    kinhte2_name = 'Ngân Hàng'
    kinhte3_name = 'Chứng Khoán'
    kinhte4_name = 'Doanh Nghiệp'
    kinhte5_name = 'Làm giàu'


    giaoduc1_name = 'Nhà Trường'
    giaoduc2_name = 'Tuyển Sinh'
    giaoduc3_name = 'Phụ huynh'
    giaoduc5_name = 'Chọn Nghề - Chọn Trường'
    
    doisong1_name = 'Người sống quanh ta'
    doisong2_name = 'Gia đình'
    doisong3_name = 'Cộng đồng'

    dulich1_name = 'Khám phá'
    dulich2_name = 'Câu chuyện du lịch'
    dulich3_name = 'Bất động sản du lịch'

    suckhoe1_name = 'Khỏe đẹp mỗi ngày'
    suckhoe2_name = 'Làm đẹp'
    suckhoe3_name = 'Giới tính'

    giaitri1_name = 'Kết nối'
    giaitri2_name = 'Truyền hình'
    giaitri3_name = 'Phim'

    thethao1_name = 'Bóng đá Việt Nam'
    thethao2_name = 'Bóng đá Quốc Tế'
    thethao3_name = 'Thể thao khác'
    
    gioitre1_name = 'Kết Nối'
    gioitre2_name = 'Khởi Nghiệp'
    gioitre3_name = 'Thế Giới Mạng'
# --------------------------------------------------
    try:
        thoisu1 = ThoiSu.objects.get(name=thoisu1_name)
    except ThoiSu.DoesNotExist:
        thoisu1 = None
    try:
        thoisu2 = ThoiSu.objects.get(name=thoisu2_name)
    except ThoiSu.DoesNotExist:
        thoisu2 = None
    try:
        thoisu3 = ThoiSu.objects.get(name=thoisu3_name)
    except ThoiSu.DoesNotExist:
        thoisu3 = None
    try:
        thoisu5 = ThoiSu.objects.get(name=thoisu5_name)
    except ThoiSu.DoesNotExist:
        thoisu5 = None  
# --------------------------------------------------
    try:
        kinhte1 = KinhTe.objects.get(name=kinhte1_name)
    except KinhTe.DoesNotExist:
        kinhte1 = None
    try:
        kinhte2 = KinhTe.objects.get(name=kinhte2_name)
    except KinhTe.DoesNotExist:
        kinhte2 = None
    try:
        kinhte3 = KinhTe.objects.get(name=kinhte3_name)
    except KinhTe.DoesNotExist:
        kinhte3 = None
    try:
        kinhte4 = KinhTe.objects.get(name=kinhte4_name)
    except KinhTe.DoesNotExist:
        kinhte4 = None
    try:
        kinhte5 = KinhTe.objects.get(name=kinhte5_name)
    except KinhTe.DoesNotExist:
        kinhte5 = None
# --------------------------------------------------
    try:
        giaoduc1 = GiaoDuc.objects.get(name=giaoduc1_name)
    except GiaoDuc.DoesNotExist:
        giaoduc1 = None
    try:
        giaoduc2 = GiaoDuc.objects.get(name=giaoduc2_name)
    except GiaoDuc.DoesNotExist:
        giaoduc2 = None
    try:
        giaoduc3 = GiaoDuc.objects.get(name=giaoduc3_name)
    except GiaoDuc.DoesNotExist:
        giaoduc3 = None
    try:
        giaoduc5 = GiaoDuc.objects.get(name=giaoduc5_name)
    except GiaoDuc.DoesNotExist:
        giaoduc5 = None
# --------------------------------------------------
    try:
        doisong1 = DoiSong.objects.get(name=doisong1_name)
    except DoiSong.DoesNotExist:
        doisong1 = None
    try:
        doisong2 = DoiSong.objects.get(name=doisong2_name)
    except DoiSong.DoesNotExist:
        doisong2 = None
    try:
        doisong3 = DoiSong.objects.get(name=doisong3_name)
    except DoiSong.DoesNotExist:
        doisong3 = None
# -------------------------------------------------- 
    try:
        dulich1 = DuLich.objects.get(name=dulich1_name)
    except DuLich.DoesNotExist:
        dulich1 = None
    try:
        dulich2 = DuLich.objects.get(name=dulich2_name)
    except DuLich.DoesNotExist:
        dulich2 = None
    try:
        dulich3 = DuLich.objects.get(name=dulich3_name)
    except DuLich.DoesNotExist:
        dulich3 = None
# --------------------------------------------------    
    try:
        suckhoe1 = SucKhoe.objects.get(name=suckhoe1_name)
    except SucKhoe.DoesNotExist:
        suckhoe1 = None
    try:
        suckhoe2 = SucKhoe.objects.get(name=suckhoe2_name)
    except SucKhoe.DoesNotExist:
        suckhoe2 = None
    try:
        suckhoe3 = SucKhoe.objects.get(name=suckhoe3_name)
    except SucKhoe.DoesNotExist:
        suckhoe3 = None
# --------------------------------------------------  
    try:
        giaitri1 = GiaiTri.objects.get(name=giaitri1_name)
    except GiaiTri.DoesNotExist:
        giaitri1 = None
    try:
        giaitri2 = GiaiTri.objects.get(name=giaitri2_name)
    except GiaiTri.DoesNotExist:
        giaitri2 = None
    try:
        giaitri3 = GiaiTri.objects.get(name=giaitri3_name)
    except GiaiTri.DoesNotExist:
        giaitri3 = None
# --------------------------------------------------      
    try:
        thethao1 = TheThao.objects.get(name=thethao1_name)
    except TheThao.DoesNotExist:
        thethao1 = None
    try:
        thethao2 = TheThao.objects.get(name=thethao2_name)
    except TheThao.DoesNotExist:
        thethao2 = None
    try:
        thethao3 = TheThao.objects.get(name=thethao3_name)
    except TheThao.DoesNotExist:
        thethao3 = None
# -------------------------------------------------- 
    try:
        gioitre1 = GioiTre.objects.get(name=gioitre1_name)
    except GioiTre.DoesNotExist:
        gioitre1 = None
    try:
        gioitre2 = GioiTre.objects.get(name=gioitre2_name)
    except GioiTre.DoesNotExist:
        gioitre2 = None
    try:
        gioitre3 = GioiTre.objects.get(name=gioitre3_name)
    except GioiTre.DoesNotExist:
        gioitre3 = None
# --------------------------------------------------      
    thoisubaiviets1 = ThoiSuBaiViet.objects.filter(thoisu=thoisu1)
    thoisubaiviets2 = ThoiSuBaiViet.objects.filter(thoisu=thoisu2)
    thoisubaiviets3 = ThoiSuBaiViet.objects.filter(thoisu=thoisu3)
    thoisubaiviets5 = ThoiSuBaiViet.objects.filter(thoisu=thoisu5)

    kinhtebaiviets1 = KinhTeBaiViet.objects.filter(kinhte=kinhte1)
    kinhtebaiviets2 = KinhTeBaiViet.objects.filter(kinhte=kinhte2)
    kinhtebaiviets3 = KinhTeBaiViet.objects.filter(kinhte=kinhte3)
    kinhtebaiviets4 = KinhTeBaiViet.objects.filter(kinhte=kinhte4)
    kinhtebaiviets5 = KinhTeBaiViet.objects.filter(kinhte=kinhte5)

    giaoducbaiviets1 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc1)
    giaoducbaiviets2 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc2)
    giaoducbaiviets3 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc3)
    giaoducbaiviets5 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc5)

    doisongbaiviets1 = DoiSongBaiViet.objects.filter(doisong=doisong1)
    doisongbaiviets2 = DoiSongBaiViet.objects.filter(doisong=doisong2)
    doisongbaiviets3 = DoiSongBaiViet.objects.filter(doisong=doisong3)

    dulichbaiviets1 = DuLichBaiViet.objects.filter(dulich=dulich1)
    dulichbaiviets2 = DuLichBaiViet.objects.filter(dulich=dulich2)
    dulichbaiviets3 = DuLichBaiViet.objects.filter(dulich=dulich3)

    suckhoebaiviets1 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe1)
    suckhoebaiviets2 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe2)
    suckhoebaiviets3 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe3)


    giaitribaiviets1 = GiaiTriBaiViet.objects.filter(giaitri=giaitri1)
    giaitribaiviets2 = GiaiTriBaiViet.objects.filter(giaitri=giaitri2)
    giaitribaiviets3 = GiaiTriBaiViet.objects.filter(giaitri=giaitri3)

    thethaobaiviets1 = TheThaoBaiViet.objects.filter(thethao=thethao1)
    thethaobaiviets2 = TheThaoBaiViet.objects.filter(thethao=thethao2)
    thethaobaiviets3 = TheThaoBaiViet.objects.filter(thethao=thethao3)

    gioitrebaiviets1 = GioiTreBaiViet.objects.filter(gioitre=gioitre1)
    gioitrebaiviets2 = GioiTreBaiViet.objects.filter(gioitre=gioitre2)
    gioitrebaiviets3 = GioiTreBaiViet.objects.filter(gioitre=gioitre3)
    
    
# --------------------------------------------------------------------------------
      


    context ={  
                'kinhtebaiviets1': kinhtebaiviets1[:3],
                'kinhtebaiviets2': kinhtebaiviets2[:3],
                'kinhtebaiviets3': kinhtebaiviets3[:3],
                'kinhtebaiviets4': kinhtebaiviets4[:4],
                 'kinhtebaiviets5': kinhtebaiviets5[:4],

                'thoisubaiviets1': thoisubaiviets1[:2],
                'thoisubaiviets2': thoisubaiviets2[:4],
                'thoisubaiviets3': thoisubaiviets3[:2],
                'thoisubaiviets5': thoisubaiviets5[:6],

                'giaoducbaiviets1': giaoducbaiviets1[:2],
                'giaoducbaiviets2': giaoducbaiviets2[:6],
                'giaoducbaiviets3': giaoducbaiviets3[:6],
                'giaoducbaiviets5': giaoducbaiviets5[:4],

                'doisongbaiviets1': doisongbaiviets1[:4],
                'doisongbaiviets2': doisongbaiviets2[:2],
                'doisongbaiviets3': doisongbaiviets3[:2],

                'dulichbaiviets1': dulichbaiviets1[:2],
                'dulichbaiviets2': dulichbaiviets2[:2],
                'dulichbaiviets3': dulichbaiviets3[:2],

                'suckhoebaiviets1': suckhoebaiviets1[:2],
                'suckhoebaiviets2': suckhoebaiviets2[:2],
                'suckhoebaiviets3': suckhoebaiviets3[:2],

                'giaitribaiviets1': giaitribaiviets1[:2],
                'giaitribaiviets2': giaitribaiviets2[:2],
                'giaitribaiviets3': giaitribaiviets3[:2],


                'thethaobaiviets1': thethaobaiviets1[:1],
                'thethaobaiviets2': thethaobaiviets2[:1],
                'thethaobaiviets3': thethaobaiviets3[:1],

                'gioitrebaiviets1': gioitrebaiviets1[:1],
                'gioitrebaiviets2': gioitrebaiviets2[:2],
                'gioitrebaiviets3': gioitrebaiviets3[:1],
                'current_time': current_time,

                'active' : 'trangchu'
                }
    return render(request, 'app/index.html', context)
def search(request):
    current_time = datetime.now()
    if request.method == 'POST':
        titel = request.POST.get('search', '')
         # Kiểm tra nếu không có từ khóa tìm kiếm được nhập
        if not titel:
            return render(request, 'app/search.html', {'error_message': 'Vui lòng nhập từ khóa để tìm kiếm'})

        if titel:
            thoisubaiviets1 = ThoiSuBaiViet.objects.filter(titel__icontains=titel)
            kinhtebaiviets1 = KinhTeBaiViet.objects.filter(titel__icontains=titel)
            dulichbaiviets1 = DuLichBaiViet.objects.filter(titel__icontains=titel)
            doisongbaiviets1 = DoiSongBaiViet.objects.filter(titel__icontains=titel)
            suckhoebaiviets1 = SucKhoeBaiViet.objects.filter(titel__icontains=titel)
            giaoducbaiviets1 = GiaoDucBaiViet.objects.filter(titel__icontains=titel)
            giaitribaiviets1 = GiaiTriBaiViet.objects.filter(titel__icontains=titel)
            thethaobaiviets1 = TheThaoBaiViet.objects.filter(titel__icontains=titel)
            gioitrebaiviets1 = GioiTreBaiViet.objects.filter(titel__icontains=titel)
        else:
            thoisubaiviets1 = []
            kinhtebaiviets1 = []
            dulichbaiviets1 = []
            doisongbaiviets1 = []
            suckhoebaiviets1 = []
            giaoducbaiviets1 = []
            giaitribaiviets1 = []
            thethaobaiviets1 = []
            gioitrebaiviets1 = []

        if not any([thoisubaiviets1, kinhtebaiviets1, dulichbaiviets1, doisongbaiviets1, suckhoebaiviets1,
                        giaoducbaiviets1, giaitribaiviets1, thethaobaiviets1, gioitrebaiviets1]):
                error_message = 'Không có kết quả phù hợp'
        else:
                error_message = None
        return render (request, 'app/search.html',
           {
                'thoisubaiviets1': thoisubaiviets1,
                'kinhtebaiviets1': kinhtebaiviets1,     
                'dulichbaiviets1': dulichbaiviets1, 
                'doisongbaiviets1': doisongbaiviets1,    
                'suckhoebaiviets1': suckhoebaiviets1,  
                'giaoducbaiviets1': giaoducbaiviets1, 
                'giaitribaiviets1': giaitribaiviets1,    
                'thethaobaiviets1': thethaobaiviets1,  
                'gioitrebaiviets1': gioitrebaiviets1,
                'error_message': 'Không có kết quả phù hợp',  # Add this line

                'current_time': current_time,
           }
        )   
    else:
        return render(request, 'app/search.html', {})

def generate_otp():
    return str(random.randint(100000, 999999))

def send_otp_email(email, otp):
    subject = 'Xác thực OTP'
    message = f'Chào mừng bạn đến với WEBSITE HỆ THỐNG THÔNG TIN ĐIỆN TỬ CMS!\n\n'
    message += f'Chúng tôi gửi đến bạn mã OTP để xác thực tài khoản của bạn:\n\n'
    message += f'Vui lòng xác thực để tiếp tục đăng nhập\n\n'
    message += f'Mã OTP của bạn là: {otp}\n\n'
    message += f'Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi.'
  
    # subject = 'Xác thực OTP'
    # message = f'Chào mừng bạn đến với WEBSITE HỆ THỐNG THÔNG TIN ĐIỆN TỬ CMS\n'
    # message = f'Mã OTP của bạn là: {otp}'
    from_email = 'truongkp703052001@mail.com'  # Thay thế bằng email của bạn hoặc tên hiển thị
    send_mail(subject, message, from_email, [email],
              
              )
  
def dangky(request):
    current_time = datetime.now()
    form = SignUpForm()

    if request.method == "POST":
        form = SignUpForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            email = form.cleaned_data.get('email')

            if User.objects.filter(username=username).exists():
                messages.error(request, 'Tên đăng nhập đã tồn tại. Vui lòng chọn tên đăng nhập khác.')
            else:
                # Tạo và lưu mã OTP vào cơ sở dữ liệu
                otp = generate_otp()
                user = form.save()

                # Tạo UserProfile và lưu vào cơ sở dữ liệu
                user_profile = UserProfile.objects.create(user=user, otp=otp, otp_expiry=datetime.now() + timedelta(minutes=5))
                  
                print(user_profile)  
                 
                # Gửi email với mã OTP
                send_otp_email(email, otp)

                messages.success(request, 'Đăng ký thành công! Mã OTP đã được gửi đến email của bạn.')
                return redirect('verify_otp', user_id=user.id)

    context = {'form': form,
               'current_time': current_time,
               }
    return render(request, 'app/dangky.html', context)


def dangnhap(request):
    current_time = datetime.now()
    if request.method=='POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request,user)
            return redirect('index')
        else:
            # return HttpResponse ("Tên đăng nhập hoặc mật khẩu không đúng!!")
            messages.error(request, 'Sai tên đăng nhập hoặc mật khẩu.')
        
    
    context = {'current_time': current_time,}
    return render(request, 'app/dangnhap.html', context)

def dangxuat(request):
    logout(request)
    return redirect('dangnhap')


def verify_otp(request, user_id):
    current_time = datetime.now()
    try:
        # Lấy thông tin người dùng từ user_id
        user_profile = UserProfile.objects.get(user_id=user_id)
    except UserProfile.DoesNotExist:
        raise Http404("UserProfile does not exist")

    if request.method == 'POST':
        entered_otp = request.POST.get('otp', '')
        saved_otp = user_profile.otp

        if entered_otp == saved_otp:
            # Xác thực thành công, chuyển hướng đến trang đăng nhập
            HttpResponse(request, 'Xác thực thành công! Đăng nhập để tiếp tục.')
            return redirect('dangnhap')
        else:
            # Xác thực thất bại, có thể hiển thị thông báo lỗi hoặc chuyển hướng đến trang khác
            messages.error(request, 'Xác thực thất bại. Mã OTP không đúng.')
            

    return render(request, 'app/verify_otp.html', {'user_id': user_id, 'current_time': current_time,})

