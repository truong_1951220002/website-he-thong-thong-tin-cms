from django.shortcuts import render
from .models import *
from .models import TheThao
from datetime import datetime

def thethao(request):
    current_time = datetime.now()
    thethao1_name = 'Bóng đá Việt Nam'
    thethao2_name = 'Bóng đá Quốc Tế'
    thethao3_name = 'Thể thao khác'
    thethao4_name = 'Bóng rổ'
    thethao5_name = 'Thể thao & Cộng đồng'
    try:
        thethao1 = TheThao.objects.get(name=thethao1_name)
    except TheThao.DoesNotExist:
        thethao1 = None
    try:
        thethao2 = TheThao.objects.get(name=thethao2_name)
    except TheThao.DoesNotExist:
        thethao2 = None
    try:
        thethao3 = TheThao.objects.get(name=thethao3_name)
    except TheThao.DoesNotExist:
        thethao3 = None
    try:
        thethao4 = TheThao.objects.get(name=thethao4_name)
    except TheThao.DoesNotExist:
        thethao4 = None
    try:
        thethao5 = TheThao.objects.get(name=thethao5_name)
    except TheThao.DoesNotExist:
        thethao5 = None
    # Lọc bài viết dựa trên đối tượng thoisu
    thethaobaiviets1 = TheThaoBaiViet.objects.filter(thethao=thethao1)
    thethaobaiviets2 = TheThaoBaiViet.objects.filter(thethao=thethao2)
    thethaobaiviets3 = TheThaoBaiViet.objects.filter(thethao=thethao3)
    thethaobaiviets4 = TheThaoBaiViet.objects.filter(thethao=thethao4)
    thethaobaiviets5 = TheThaoBaiViet.objects.filter(thethao=thethao5)
    context = {
        'thethaobaiviets1': thethaobaiviets1[:4],
        'thethaobaiviets2': thethaobaiviets2[:6],
        'thethaobaiviets3': thethaobaiviets3[:6],
        'thethaobaiviets4': thethaobaiviets4[:6],
        'thethaobaiviets5': thethaobaiviets5[:6],
        'thethao1': thethao1_name,
        'thethao2': thethao2_name,
        'thethao3': thethao3_name,
        'thethao4': thethao4_name,
        'thethao5': thethao5_name,
        'active' : 'thethao',
        'current_time': current_time,
    }
    return render(request, 'thethao/thethao.html', context)

def detail(request, slug):
    current_time = datetime.now()
    suckhoe1_name = 'Khỏe đẹp mỗi ngày'
    doisong1_name = 'Người sống quanh ta'
    suckhoe2_name = 'Làm đẹp'
    kinhte3_name = 'Chứng Khoán'
    try:
        suckhoe1 = SucKhoe.objects.get(name=suckhoe1_name)
    except SucKhoe.DoesNotExist:
        suckhoe1 = None
    try:
        doisong1 = DoiSong.objects.get(name=doisong1_name)
    except DoiSong.DoesNotExist:
        doisong1 = None
    try:
        suckhoe2 = SucKhoe.objects.get(name=suckhoe2_name)
    except SucKhoe.DoesNotExist:
        suckhoe2 = None   
    try:
        kinhte3 = KinhTe.objects.get(name=kinhte3_name)
    except KinhTe.DoesNotExist:
        kinhte3 = None         
    if TheThaoBaiViet.objects.filter(slug=slug).exists():
        thethaobaiviets = TheThaoBaiViet.objects.filter(slug=slug).first()
        suckhoebaiviets1 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe1)
        doisongbaiviets1 = DoiSongBaiViet.objects.filter(doisong=doisong1)
        suckhoebaiviets2 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe2)
        kinhtebaiviets3 = KinhTeBaiViet.objects.filter(kinhte=kinhte3)
        context= {'data': thethaobaiviets,
                  'suckhoebaiviets1': suckhoebaiviets1[:2],
                  'doisongbaiviets1': doisongbaiviets1[:2],
                  'suckhoebaiviets2': suckhoebaiviets2[:6],
                'kinhtebaiviets3': kinhtebaiviets3[:5],
                  'active' : 'thethao',
                  'current_time': current_time,
                  }  

    return render(request, 'app/detail.html',context)