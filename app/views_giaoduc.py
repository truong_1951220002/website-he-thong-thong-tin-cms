from django.shortcuts import render
from .models import *
from .models import GiaoDuc
from datetime import datetime
def giaoduc(request):
    current_time = datetime.now()
    giaoduc1_name = 'Nhà Trường'
    giaoduc2_name = 'Tuyển Sinh'
    giaoduc3_name = 'Phụ huynh'
    giaoduc4_name = 'Du học'
    giaoduc5_name = 'Chọn Nghề - Chọn Trường'
    try:
        giaoduc1 = GiaoDuc.objects.get(name=giaoduc1_name)
    except GiaoDuc.DoesNotExist:
        giaoduc1 = None
    try:
        giaoduc2 = GiaoDuc.objects.get(name=giaoduc2_name)
    except GiaoDuc.DoesNotExist:
        giaoduc2 = None
    try:
        giaoduc3 = GiaoDuc.objects.get(name=giaoduc3_name)
    except GiaoDuc.DoesNotExist:
        giaoduc3 = None
    try:
        giaoduc4 = GiaoDuc.objects.get(name=giaoduc4_name)
    except GiaoDuc.DoesNotExist:
        giaoduc4 = None
    try:
        giaoduc5 = GiaoDuc.objects.get(name=giaoduc5_name)
    except GiaoDuc.DoesNotExist:
        giaoduc5 = None
    # Lọc bài viết dựa trên đối tượng thoisu
    giaoducbaiviets1 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc1)
    giaoducbaiviets2 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc2)
    giaoducbaiviets3 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc3)
    giaoducbaiviets4 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc4)
    giaoducbaiviets5 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc5)
    context = {
        'giaoducbaiviets1': giaoducbaiviets1[:4],
        'giaoducbaiviets2': giaoducbaiviets2[:6],
        'giaoducbaiviets3': giaoducbaiviets3[:6],
        'giaoducbaiviets4': giaoducbaiviets4[:6],
        'giaoducbaiviets5': giaoducbaiviets5[:6],
        'giaoduc1': giaoduc1_name,
        'giaoduc2': giaoduc2_name,
        'giaoduc3': giaoduc3_name,
        'giaoduc4': giaoduc4_name,
        'giaoduc5': giaoduc5_name,
        'active' : 'giaoduc',
        'current_time': current_time,
        
    }
    return render(request, 'giaoduc/giaoduc.html', context)

def detail(request, slug):
    current_time = datetime.now()
    doisong1_name = 'Người sống quanh ta'
    gioitre1_name = 'Kết Nối'
    gioitre2_name = 'Khởi Nghiệp'
    thoisu3_name = 'Dân sinh'
    giaitri2_name = 'Truyền hình'
    thoisu2_name = 'Pháp luật'
    try:
        doisong1 = DoiSong.objects.get(name=doisong1_name)
    except DoiSong.DoesNotExist:
        doisong1 = None
    try:
        gioitre1 = GioiTre.objects.get(name=gioitre1_name)
    except GioiTre.DoesNotExist:
        gioitre1 = None
    try:
        gioitre2 = GioiTre.objects.get(name=gioitre2_name)
    except GioiTre.DoesNotExist:
        gioitre2 = None
    try:
        thoisu3 = ThoiSu.objects.get(name=thoisu3_name)
    except ThoiSu.DoesNotExist:
        thoisu3 = None 
    try:
        giaitri2 = GiaiTri.objects.get(name=giaitri2_name)
    except GiaiTri.DoesNotExist:
        giaitri2 = None
    try:
        thoisu2 = ThoiSu.objects.get(name=thoisu2_name)
    except ThoiSu.DoesNotExist:
        thoisu2 = None
    if GiaoDucBaiViet.objects.filter(slug=slug).exists():
        giaoducbaiviets = GiaoDucBaiViet.objects.filter(slug=slug).first()
        doisongbaiviets1 = DoiSongBaiViet.objects.filter(doisong=doisong1)
        gioitrebaiviets1 = GioiTreBaiViet.objects.filter(gioitre=gioitre1)
        giaitribaiviets2 = GiaiTriBaiViet.objects.filter(giaitri=giaitri2)
        gioitrebaiviets2 = GioiTreBaiViet.objects.filter(gioitre=gioitre2)
        thoisubaiviets2 = ThoiSuBaiViet.objects.filter(thoisu=thoisu2)
        thoisubaiviets3 = ThoiSuBaiViet.objects.filter(thoisu=thoisu3)
        context= {'data': giaoducbaiviets,
                    'gioitrebaiviets1': gioitrebaiviets1[:2],
                    'doisongbaiviets1': doisongbaiviets1[:2],
                    'giaitribaiviets2': giaitribaiviets2[:6],
                    'gioitrebaiviets2': gioitrebaiviets2[:4],
                    'thoisubaiviets3': thoisubaiviets3[:5],
                    'thoisubaiviets2': thoisubaiviets2[:6],
                    'active' : 'giaoduc',
                    'current_time': current_time,
                       }  

    return render(request, 'app/detail.html',context)