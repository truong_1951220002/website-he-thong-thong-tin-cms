from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import ChangePasswordForm
from django.contrib.auth import update_session_auth_hash
from django.http import JsonResponse
from django.core.files.storage import default_storage
from .forms import UserProfileForm
from datetime import datetime
# def profile(request):
#     user = request.user

#     if user.is_authenticated:
#         email = user.email
#     else:
#         # Xử lý trường hợp người dùng chưa đăng nhập
#         email = None 

#     context = {'email': email}
#     return render(request, 'profile/profile.html', context)
# def profile(request):
#     # Kiểm tra xem người dùng đã đăng nhập hay chưa
#     if request.user.is_authenticated:
#         user = request.user
#         email = user.email
#         profile_image_url = user.userprofile.profile_image.url if hasattr(user, 'userprofile') and user.userprofile.profile_image else None
#     else:
#         # Trường hợp người dùng chưa đăng nhập
#         email = None
#         profile_image_url = None

#     context = {
#         'email': email,
#         'profile_image_url': profile_image_url,
#     }

#     return render(request, 'profile/profile.html', context)


def profile(request):
    current_time = datetime.now()
    user = request.user
    email = user.email

    # Lấy hoặc tạo một instance của UserProfile
    user_profile, created = UserProfile.objects.get_or_create(user=user)

    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=user_profile)
        if form.is_valid():
            form.save()
            # Chuyển hướng hoặc làm bất kỳ điều gì sau khi cập nhật thành công
            return redirect('profile')  # Thay 'profile' bằng tên đường dẫn của trang profile của bạn
    else:
        form = UserProfileForm(instance=user_profile)

    context = {'user': user, 'email': email, 'form': form,
               'current_time': current_time,
               }

    return render(request, 'profile/profile.html', context)
@login_required
def doimatkhau(request):
    current_time = datetime.now()
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            current_password = form.cleaned_data['current_password']
            new_password = form.cleaned_data['new_password']
            confirm_password = form.cleaned_data['confirm_password']

            # Kiểm tra xác nhận mật khẩu mới
            if new_password != confirm_password:
                messages.error(request, "Mật khẩu mới và xác nhận mật khẩu không khớp.")
                return redirect('doimatkhau')

            # Kiểm tra mật khẩu hiện tại
            if not request.user.check_password(current_password):
                messages.error(request, "Mật khẩu hiện tại không đúng.")
                return redirect('doimatkhau')

            # Cập nhật mật khẩu mới
            request.user.set_password(new_password)
            request.user.save()

            # Cập nhật session auth hash để tránh đăng xuất người dùng
            update_session_auth_hash(request, request.user)

            messages.success(request, "Đổi mật khẩu thành công.")
            return redirect('doimatkhau')
        else:
            messages.error(request, "Vui lòng điền đúng thông tin.")
    else:
        form = ChangePasswordForm()
    user = request.user
    email = user.email

    context = {'form': form,
            'email': email,
               'current_time': current_time,
               }
    return render(request, 'profile/doimatkhau.html', context)




@login_required
def save_profile(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            user = request.user
            user.first_name = data.get('first_name')
            user.last_name = data.get('last_name')
            user.phone_number = data.get('phone_number')
            user.save()
            return JsonResponse({'status': 'success'})
        except json.JSONDecodeError:
            return JsonResponse({'status': 'error', 'message': 'Dữ liệu không hợp lệ'})
    else:
        return JsonResponse({'status': 'error', 'message': 'Yêu cầu không hợp lệ'})
    



def luutrutin(request, ):
    if request.method == 'POST' and request.is_ajax():
        # Lấy dữ liệu từ POST request
        slug = request.POST.get('slug', '')
        # Thêm các trường dữ liệu khác cần thiết từ form

        # Xác định bài viết cần lưu thông tin
        thoisubaiviets1 = ThoiSuBaiViet.objects.get(slug=slug)

        # Thực hiện cập nhật dữ liệu bài viết
        # Ví dụ: thoisu_baiviet.field_name = request.POST.get('field_name')
        # thoisu_baiviet.save()

        # Trả về JsonResponse để thông báo cho client rằng lưu thành công
        return JsonResponse({'message': 'Lưu thành công'})

    # Nếu không phải là phương thức POST hoặc không phải là AJAX request, trả về lỗi
    return JsonResponse({'message': 'Invalid request'}, status=400)