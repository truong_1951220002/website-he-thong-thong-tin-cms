from django.shortcuts import render
from .models import *
from .models import GioiTre
from datetime import datetime
def gioitre(request):
    current_time = datetime.now()
    gioitre1_name = 'Kết nối'
    gioitre2_name = 'Khởi nghiệp'
    gioitre3_name = 'Thế giới mạng'
    gioitre4_name = 'Cơ hội nghề nghiệp'
    gioitre5_name = 'Đoàn - Hội'


    try:
        gioitre1 = GioiTre.objects.get(name=gioitre1_name)
    except GioiTre.DoesNotExist:
        gioitre1 = None
    try:
        gioitre2 = GioiTre.objects.get(name=gioitre2_name)
    except GioiTre.DoesNotExist:
        gioitre2 = None
    try:
        gioitre3 = GioiTre.objects.get(name=gioitre3_name)
    except GioiTre.DoesNotExist:
        gioitre3 = None
    try:
        gioitre4 = GioiTre.objects.get(name=gioitre4_name)
    except GioiTre.DoesNotExist:
        gioitre4 = None
    try:
        gioitre5 = GioiTre.objects.get(name=gioitre5_name)
    except GioiTre.DoesNotExist:
        gioitre5 = None
    # Lọc bài viết dựa trên đối tượng thoisu
    gioitrebaiviets1 = GioiTreBaiViet.objects.filter(gioitre=gioitre1)
    gioitrebaiviets2 = GioiTreBaiViet.objects.filter(gioitre=gioitre2)
    gioitrebaiviets3 = GioiTreBaiViet.objects.filter(gioitre=gioitre3)
    gioitrebaiviets4 = GioiTreBaiViet.objects.filter(gioitre=gioitre4)
    gioitrebaiviets5 = GioiTreBaiViet.objects.filter(gioitre=gioitre5)
    context = {
        'gioitrebaiviets1': gioitrebaiviets1[:4],
        'gioitrebaiviets2': gioitrebaiviets2[:6],
        'gioitrebaiviets3': gioitrebaiviets3[:6],
        'gioitrebaiviets4': gioitrebaiviets4[:6],
        'gioitrebaiviets5': gioitrebaiviets5[:6],
        'gioitre1': gioitre1_name,
        'gioitre2': gioitre2_name,
        'gioitre3': gioitre3_name,
        'gioitre4': gioitre4_name,
        'gioitre5': gioitre5_name,
         'active' : 'gioitre',
         'current_time': current_time,
    }
    return render(request, 'gioitre/gioitre.html', context)

def detail(request, slug):
    current_time = datetime.now()
    suckhoe1_name = 'Khỏe đẹp mỗi ngày'
    giaitri1_name = 'Kết nối'
    giaitri2_name = 'Truyền hình'
    dulich3_name = 'Bất động sản du lịch'
    giaoduc2_name = 'Tuyển Sinh'
    try:
        suckhoe1 = SucKhoe.objects.get(name=suckhoe1_name)
    except SucKhoe.DoesNotExist:
        suckhoe1 = None
    try:
        giaitri1 = GiaiTri.objects.get(name=giaitri1_name)
    except GiaiTri.DoesNotExist:
        giaitri1 = None    
    try:
        giaitri2 = GiaiTri.objects.get(name=giaitri2_name)
    except GiaiTri.DoesNotExist:
        giaitri2 = None
    try:
        dulich3 = DuLich.objects.get(name=dulich3_name)
    except DuLich.DoesNotExist:
        dulich3 = None
    try:
        giaoduc2 = GiaoDuc.objects.get(name=giaoduc2_name)
    except GiaoDuc.DoesNotExist:
        giaoduc2 = None
    if GioiTreBaiViet.objects.filter(slug=slug).exists():
        gioitrebaiviets = GioiTreBaiViet.objects.filter(slug=slug).first()
        suckhoebaiviets1 = SucKhoeBaiViet.objects.filter(suckhoe=suckhoe1)
        giaoducbaiviets2 = GiaoDucBaiViet.objects.filter(giaoduc=giaoduc2)
        giaitribaiviets1 = GiaiTriBaiViet.objects.filter(giaitri=giaitri1)
        giaitribaiviets2 = GiaiTriBaiViet.objects.filter(giaitri=giaitri2)
        dulichbaiviets3 = DuLichBaiViet.objects.filter(dulich=dulich3)
        context= {'data': gioitrebaiviets,
                  'suckhoebaiviets1': suckhoebaiviets1[:2],
                  'giaitribaiviets1': giaitribaiviets1[:2],
                  'giaoducbaiviets2': giaoducbaiviets2[:6],
                  'giaitribaiviets2': giaitribaiviets2[:4],
                    'dulichbaiviets3': dulichbaiviets3[:5],
                    'active' : 'gioitre',
                    'current_time': current_time,
                  }  

    return render(request, 'app/detail.html',context)